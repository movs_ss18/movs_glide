package com.example.steffen.movs_glide;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.*;
import com.bumptech.glide.request.RequestOptions;

public class MainActivity extends AppCompatActivity {

    ImageView iv1, iv2, iv3, iv4;
    Button bt_switch;
    Context c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        c = this;

        iv1 = (ImageView) findViewById(R.id.iv1);
        iv2 = (ImageView) findViewById(R.id.iv2);
        iv3 = (ImageView) findViewById(R.id.iv3);
        iv4 = (ImageView) findViewById(R.id.iv4);

        bt_switch = (Button) findViewById(R.id.button);
        bt_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(c, Main2Activity.class);
                c.startActivity(i);
            }
        });

        //simple Image
        Glide.with(this).load("http://www.melanche.de/wp-content/uploads/2015/04/katzenbaby-1000x600.jpg").into(iv1);

        //GIF
        Glide.with(this).load("https://media.giphy.com/media/3oEduGi1UWg9Q6nF84/giphy.gif").apply(new RequestOptions().centerCrop()).into(iv2);

        //GIF+Center
        Glide.with(this).load("https://media.giphy.com/media/3oEduGi1UWg9Q6nF84/giphy.gif").apply(new RequestOptions().placeholder(R.drawable.wait2)).into(iv3);


        Glide.with(this).load("https://media.giphy.com/media/3oEduGi1UWg9Q6nF84/giphy.gif").thumbnail(0.1f).apply(new RequestOptions().error(R.drawable.error)).into(iv4);

    }
}
